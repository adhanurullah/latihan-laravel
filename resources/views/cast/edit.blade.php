@extends('layout.master')

@section('judul')
    Halaman Edit Cast
@endsection

@section('content')
<form action="/cast/{{$cast->id}}" method="POST">
    @csrf
    @method('put')
    <div class="form-group">
      <label> Name</label>
      <input type="string" value="{{$cast->name}}" name="name" class="form-control">
    </div>
    @error('name')
    <div class="alert alert-danger">{{ $message }}</div>
@enderror
    <div class="form-group">
      <label >umur</label>
      <input type="integer" value="{{$cast->name}}" name="umur"  class="form-control">
    </div>
    @error('umur')
    <div class="alert alert-danger">{{ $message }}</div>
@enderror
    <div class="form-group">
        <label >Bio</label>
        <input type="text" value="{{$cast->name}}" name= "bio" class="form-control">
      </div>
      @error('bio')
    <div class="alert alert-danger">{{ $message }}</div>
@enderror
    <button type="submit" class="btn btn-primary">Submit</button>
  </form>
@endsection