<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/','HomeController@home');
Route::get('/register','AuthController@register');
Route::post('/welcome','AuthController@welcome');

Route::get('/data-table', function(){
    return view('halaman.data-table');
});

Route::get('/table', function(){
    return view('halaman.table');
});

// CRUD Cast

//Create
//Form input data create cast
Route::get('/cast/create','castController@create');
Route::post('/cast','castController@store');

// read
// menampilkan semua data di table cast
Route::get('/cast', 'castController@index');

// menampilkan detaill cast berdasarkan id
Route::get('cast/{cast_id}', 'castController@show');

//update
//form edit data cast
Route::get('/cast/{cast_id}/edit', 'castController@edit');

//untuk update data berdasarkan id
Route::put('/cast/{cast_id}', 'castController@update');

//delete
Route::delete('/cast/{cast_id}', 'castController@destroy');